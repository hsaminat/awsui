import { Component, OnInit } from '@angular/core';
import { GridOptions, Module, GridApi, AllModules, ColumnApi } from '@ag-grid-enterprise/all-modules';
import { CommonStoreService } from './common-store.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  normalizedLogData: any;
  title = 'Road Runner';
  statusBar
  public rowData: any[];
  public columnDefs: any[];
  public rowCount: string;
  public defaultColDef: any;
  public frameworkComponents: any;
  public sideBar: false;
  public modules: Module[] = AllModules;
  public api: GridApi;
  public columnApi: ColumnApi;
  data: any = []
  selectedOption: string ="NORMALIZED";
  constructor(private commonStoreService: CommonStoreService) { }
  sample = {
    "denormalizedLogData": [
      {
        "odsClient": "HCI",
        "productCode": "HYVS",
        "rateSystem": "CLIP States",
        "priceMajorVersion": 10042,
        "pricerevisionbatchid": 0,
        "publishRate": "true",
        "status": "NEW",
        "crDt": "2020-12-09T08:28:20.202Z"
      },
      {
        "odsClient": "HCI",
        "productCode": "HYVS",
        "rateSystem": "CLIP States",
        "priceMajorVersion": 10042,
        "pricerevisionbatchid": 0,
        "publishRate": "true",
        "status": "COMPLETE",
        "crDt": "2020-12-09T08:28:20.202Z"
      }
    ],
    "normalizedLogData": [
      {
        "pricinglogid": 10071,
        "pricerevisionbatchid": 202,
        "pricerevisionid": 66579,
        "logcode": "LOG",
        "logdesc": "Started Pricing",
        "createddate": "2020-12-07 21:58:29.779342"
      },
      {
        "pricinglogid": 10071,
        "pricerevisionbatchid": 202,
        "pricerevisionid": 66579,
        "logcode": "LOG",
        "logdesc": "Finished Pricing",
        "createddate": "2020-12-07 21:59:29.779342"
      }
    ]
  }
  selectedRecords: number = 100;
  istableVisible: boolean
  headTitle: string;
  ngOnInit(): void {

  }

  onClickingButton() {
    this.istableVisible = true;
    
    if(this.selectedOption === 'NORMALIZED'){
      this.headTitle = `Road runner - Process B log events for last ${this.selectedRecords} records`
    }else{
      this.headTitle = `Road runner - Process E log events for last ${this.selectedRecords} records`
    }
    

    this.commonStoreService.getData(this.selectedOption, this.selectedRecords).subscribe(response => {
      console.log(response)
      this.createColumnDefs(response)
      this.data = response
    })
    // this.commonStoreService.getData(this.selectedOption,this.selectedRecords).subscribe(response => console.log(response))

  }

  createColumnDefs(source) {
    let columns = [];
    let columnHeader = source[0];
    for (let value in columnHeader) {
      let eachColumn = {};
      eachColumn['headerName'] = value.toLocaleUpperCase()
      eachColumn['field'] = value
      columns.push(eachColumn)
    }
    this.columnDefs = columns
    this.defaultColDef = {
      resizable: true,
      sortable: true,
      filter: true,
      editable: true,
    };
    this.statusBar = {
      statusPanels: [
        {
          statusPanel: 'agTotalAndFilteredRowCountComponent',
          align: 'left',
        }]
    }
  }

}
