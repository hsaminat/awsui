import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CommonStoreService {

  public static readonly baseURL = 'https://koix42i0rb.execute-api.us-east-2.amazonaws.com/dev/utility/log/';
  
  constructor(private http : HttpClient) { }

  getData(option,fetchRecords){
    let concatenatedString = CommonStoreService.baseURL+ `${option}?fetch=${fetchRecords}`;
    let responseSource = null;
    if(option === "NORMALIZED"){
      responseSource = 'normalizedLogData'
    }else{
      responseSource = 'denormalizedLogData'
    }
    console.log(concatenatedString)
    return this.http.get(concatenatedString).pipe(
      map(response => response[responseSource])
    )
  }
}
